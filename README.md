# TextClassification

For the moment only following classes are supported:

classes = ["date", "analyst name", "time stamp", "test bench", "finding", "unknown"]

Important attributes:

sentence_max_lenght = 128
- that limits the size of the tokenizer and input layer size, consequently the size of the model

Pretrained model will be provided over cloud.

## Getting started

```
pip install -r requirements.txt
```

## Missing
- [ ] TBD
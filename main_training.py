import tensorflow as tf
print(tf.version.VERSION)
import pandas as pd
import numpy as np
from sklearn.model_selection import KFold

# %config IPCompleter.greedy=True
# %config IPCompleter.use_jedi=False
# nltk.download('twitter_samples')
# https://github.com/philipperemy/keras-tcn
# https://github.com/diardanoraihan/Text_Classification_Capstone

# Import the library in your Jupyter notebook
import jovian
import os
from tqdm import tqdm

tf.config.list_physical_devices('GPU')


def split_sentence(_sentence, _max_characters):
    splited_empty_space = _sentence.split(" ")
    splited_empty_space_len = [len(word) for word in splited_empty_space]
    sum_of_lenght = 0
    list_of_indices = list()
    for index, word_lenght in enumerate(splited_empty_space_len):
        sum_of_lenght += word_lenght + 1
        if sum_of_lenght > _max_characters:
            list_of_indices.append(index - 1)
            sum_of_lenght = 0

    finding_concatanated = ""
    splited_sentences = list()
    first_index = 0
    for parsing_index in list_of_indices:
        for text in splited_empty_space[first_index:parsing_index]:
            finding_concatanated += text + " "
            first_index = parsing_index
        splited_sentences.append(finding_concatanated[:len(finding_concatanated)-1])
        finding_concatanated = ""

    return splited_sentences

# https://arxiv.org/ftp/arxiv/papers/2107/2107.03158.pdf
# TBD
# clean the dataset
# - remove special characters
# - replace german to english - encoding?
def clean_text(text):
    text = text.replace("#", "")
    text = text.replace("\n", " ")
    text = text.strip()
    return text

# https://github.com/dsfsi/textaugment#Installation
# pip install textaugment
import nltk
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
nltk.download('omw-1.4')
nltk.download('punkt')
nltk.download('wordnet')
from textaugment import Wordnet
t_wordnet = Wordnet()

from textaugment import EDA
t_eda = EDA()


def augment_text(text, entries, label, train):
    cln_text = clean_text(text)
    if is_not_empty_string(cln_text):

        entries.append([t_wordnet.augment(cln_text), label, train])

        # Synonym Replacement
        # t_synonym = t_eda.synonym_replacement(text)
        entries.append([t_eda.synonym_replacement(cln_text), label, train])

        # t_random_deletion = t_eda.random_deletion(text, p=0.2)
        entries.append([t_eda.random_deletion(cln_text, p=0.2), label, train])

        # t_swap = t_eda.random_swap(text)
        entries.append([t_eda.random_swap(cln_text), label, train])

        # t_random_insertion = t_eda.random_insertion(text)
        entries.append([t_eda.random_insertion(cln_text), label, train])


def is_not_empty_string(text):
    return text != "" and text != " " and len(text) != 0


classes = ["date", "analyst name", "time stamp", "test bench", "finding", "unknown"]
data_in_dict = pd.read_pickle(os.path.join('data', 'word_to_vec_input_dict.p'))
entries = list()
full_text = list()
max_characters = 128
empty_list = [0] * len(classes)

for index, (ticket_ids, text_fields) in enumerate(tqdm(data_in_dict.items())):
    entries.append([clean_text(text_fields["date"]), [1, 0, 0, 0, 0, 0], "train"])
    augment_text(text_fields["analyst_name"], entries, [0, 1, 0, 0, 0, 0], "train")
    augment_text(text_fields["All relevant error timestamps:"], entries, [0, 0, 1, 0, 0, 0], "train")
    augment_text(text_fields["Test bench:"], entries, [0, 0, 0, 1, 0, 0], "train")

    incorporate_findings = True
    if incorporate_findings:
        if "\n\n" in text_fields["Findings:"]:
            splited_finding = text_fields["Findings:"].split("\n\n")
            for finding in splited_finding:
                if is_not_empty_string(finding):
                    if len(finding) < max_characters:
                        augment_text(finding, entries, [0, 0, 0, 0, 1, 0], "train")
                    else:
                        list_of_splited_sentences = split_sentence(finding, max_characters)
                        for splited_sentence in list_of_splited_sentences:
                            augment_text(splited_sentence, entries, [0, 0, 0, 0, 1, 0], "train")

        else:
            if len(finding) < max_characters:
                augment_text(finding, entries, [0, 0, 0, 0, 1, 0], "train")
            else:
                list_of_splited_sentences = split_sentence(finding, max_characters)
                for splited_sentence in list_of_splited_sentences:
                    augment_text(splited_sentence, entries, [0, 0, 0, 0, 1, 0], "train")

add_unknown = False
if add_unknown:
    for index, (ticket_ids, text_fields) in enumerate(tqdm(data_in_dict.items())):
        temp_entries = [clean_text(text_fields["date"]), clean_text(text_fields["analyst_name"]),
                        clean_text(text_fields["All relevant error timestamps:"]),
                        clean_text(text_fields["Test bench:"]), clean_text(text_fields["Findings:"])]
        parsed_after_newline = text_fields["plain_text"].split("\n\n")

        # choosing 10, which means already that the dataset will unbalanced
        if len(parsed_after_newline) > 0:
            masking_random_text = np.random.randint(0, len(parsed_after_newline), 1)
            for random_number in masking_random_text:
                line_after_line = clean_text(parsed_after_newline[random_number])
                if is_not_empty_string(line_after_line) and line_after_line not in temp_entries:
                    if len(line_after_line) > (max_characters - 1):
                        shortened_line = line_after_line[:(max_characters - 1)].rsplit(' ', 1)[0]
                        entries.append([shortened_line, [0, 0, 0, 0, 0, 1], "train"])
                    else:
                        entries.append([line_after_line, [0, 0, 0, 0, 0, 1], "train"])
                else:
                    print("found in the previouse entries: {}".format(line_after_line))
        full_text.append(text_fields["plain_text"])

corpus = pd.DataFrame(entries, columns=["sentence", "label", "split"])
corpus.to_pickle(os.path.join('data', 'training_testing_dataset.p'))
# corpus.label = corpus.label.astype(int)

print(corpus.shape)

corpus.info()
# corpus.groupby(by='label').count()

# Separate the sentences and the labels
sentences, labels = list(corpus.sentence), list(corpus.label)


# Define a function to compute the max length of sequence
def max_length(sequences):
    '''
    input:
        sequences: a 2D list of integer sequences
    output:
        max_length: the max length of the sequences
    '''
    max_length = 0
    for i, seq in enumerate(sequences):
        length = len(seq)
        if max_length < length:
            max_length = length
    return max_length


from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

trunc_type = 'post'
padding_type = 'post'
oov_tok = "<UNK>"

print("Example of sentence: ", sentences[4])

# Cleaning and Tokenization
tokenizer = Tokenizer(oov_token=oov_tok)
tokenizer.fit_on_texts(sentences)
tokenize_conf = tokenizer.to_json()
import json
with open(os.path.join('data', 'tokenizer_conf.json'), 'w') as f:
    json.dump(tokenize_conf, f)

# Turn the text into sequence
training_sequences = tokenizer.texts_to_sequences(sentences)
max_len = max_length(training_sequences)

print('Into a sequence of int:', training_sequences[4])

# Pad the sequence to have the same size
training_padded = pad_sequences(training_sequences, maxlen=max_len, padding=padding_type, truncating=trunc_type)
print('Into a padded sequence:', training_padded[4])

word_index = tokenizer.word_index
# See the first 10 words in the vocabulary
for i, word in enumerate(word_index):
    print(word, word_index.get(word))
    if i == 9:
        break
vocab_size = len(word_index) + 1
print("Vocabulary size {}".format(vocab_size))

from tcn import TCN, tcn_full_summary
from tensorflow.keras.layers import Input, Embedding, Dense, Dropout, SpatialDropout1D
from tensorflow.keras.layers import concatenate, GlobalAveragePooling1D, GlobalMaxPooling1D
from tensorflow.keras.models import Model

def define_model(optimizer, kernel_size=3, activation='relu', input_dim=None, output_dim=300, max_length=None):
    inp = Input(shape=(max_length,))
    x = Embedding(input_dim=input_dim, output_dim=output_dim, input_length=max_length)(inp)
    x = SpatialDropout1D(0.1)(x)

    x = TCN(128, dilations=[1, 2, 4], kernel_size=kernel_size, return_sequences=True, dropout_rate=0.1,
            activation=activation)(x)
    x = TCN(64, dilations=[1, 2, 4], kernel_size=kernel_size, return_sequences=True, dropout_rate=0.1,
            activation=activation)(x)

    avg_pool = GlobalAveragePooling1D()(x)
    max_pool = GlobalMaxPooling1D()(x)

    conc = concatenate([avg_pool, max_pool])
    conc = Dense(32, activation="relu")(conc)
    conc = Dropout(0.1)(conc)
    outp = Dense(6, activation="sigmoid")(conc)

    model = Model(inputs=inp, outputs=outp)
    model.compile(loss=tf.keras.losses.BinaryFocalCrossentropy(), optimizer=optimizer, metrics=['accuracy'])

    model.summary()

    return model


# class myCallback(tf.keras.callbacks.Callback):
#     # Overide the method on_epoch_end() for our benefit
#     def on_epoch_end(self, epoch, logs={}):
#         if (logs.get('accuracy') > 0.93):
#             print("\nReached 93% accuracy so cancelling training!")
#             self.model.stop_training=True


callbacks = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy', min_delta=0,
                                             patience=10, verbose=2,
                                             mode='auto', restore_best_weights=True)

# Parameter Initialization
trunc_type = 'post'
padding_type = 'post'
oov_tok = "<UNK>"
activations = ['relu', 'tanh']
kernel_sizes = [1, 3, 5]

# https://github.com/Tony607/Keras_Bag_of_Tricks/blob/master/warmup_cosine_decay_scheduler.py
optimizer = tf.keras.optimizers.SGD(
    learning_rate=0.01, momentum=0.0, nesterov=True, name="SGD"
)
optimizer = tf.keras.optimizers.Nadam(
    learning_rate=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-07, name="Nadam"
)


columns = ['Activation', 'Filters', 'acc1', 'acc2', 'acc3', 'acc4', 'acc5', 'acc6', 'acc7', 'acc8', 'acc9', 'acc10',
           'AVG']
record = pd.DataFrame(columns=columns)

# prepare cross validation with 10 splits and shuffle = True
kfold = KFold(n_splits=10, shuffle=True, random_state=None)

exp = 0
batch_size = 128
epochs = 15

for activation in activations:
    for kernel_size in kernel_sizes:
        checkpoint_path = str(activation) + "_" + str(kernel_size) + "_model.h5"

        # kfold.split() will return set indices for each split
        exp += 1
        print('-------------------------------------------')
        print('Training {}: {} activation, {} kernel size.'.format(exp, activation, kernel_size))
        print('-------------------------------------------')
        acc_list = list()
        predictions = list()
        for train, test in kfold.split(sentences):

            train_x, test_x = [], []
            train_y, test_y = [], []

            for i in train:
                train_x.append(sentences[i])
                train_y.append(labels[i])

            for i in test:
                test_x.append(sentences[i])
                test_y.append(labels[i])

            # Turn the labels into a numpy array
            train_y = np.array(train_y)
            test_y = np.array(test_y)

            # encode data using
            # Cleaning and Tokenization
            tokenizer = Tokenizer(oov_token=oov_tok)
            tokenizer.fit_on_texts(train_x)

            # Turn the text into sequence
            training_sequences = tokenizer.texts_to_sequences(train_x)
            test_sequences = tokenizer.texts_to_sequences(test_x)

            max_len = max_length(training_sequences)

            # Pad the sequence to have the same size
            Xtrain = pad_sequences(training_sequences, maxlen=max_len, padding=padding_type, truncating=trunc_type)
            Xtest = pad_sequences(test_sequences, maxlen=max_len, padding=padding_type, truncating=trunc_type)

            word_index = tokenizer.word_index
            vocab_size = len(word_index) + 1

            # Define the input shape
            model = define_model(optimizer, kernel_size, activation, input_dim=vocab_size, max_length=max_len)

            # Train the model
            model.fit(Xtrain, train_y, batch_size=batch_size, epochs=epochs, verbose=1,
                      callbacks=[callbacks], validation_data=(Xtest, test_y))

            # evaluate the model
            loss, acc = model.evaluate(Xtest, test_y, verbose=0)
            print('Test Accuracy: {}'.format(acc * 100))

            outputs = model.predict(Xtest)
            for x_test_text, output in zip(test_x, outputs):
                predictions.append([x_test_text, classes[np.argmax(output)], np.max(output), output])

            acc_list.append(acc * 100)

        if add_unknown:
            predictions = list()
            print("Predictions running:")
            for unseen_text in tqdm(full_text):
                parsed_after_newline = unseen_text.split("\n\n")
                for line_after_line in parsed_after_newline:
                    if is_not_empty_string(line_after_line):
                        seq = tokenizer.texts_to_sequences([clean_text(line_after_line)])
                        padded = pad_sequences(seq, maxlen=max_len, padding=padding_type, truncating=trunc_type)
                        output = model.predict(padded)
                        predictions.append([clean_text(line_after_line), classes[np.argmax(output)], np.max(output), output])

        # https://www.tensorflow.org/api_docs/python/tf/keras/models/save_model
        model.save(os.path.join("data", "acc_" + str(acc) + "_" + checkpoint_path), save_format='h5') # overwrite=True

        predictions_df = pd.DataFrame(predictions, columns=["sentence", "label", "probability", "all probabilities"])
        predictions_df.to_csv(os.path.join("data", "acc_" + str(acc) + "_" + str(activation) + "_" + str(
            kernel_size) + "_predictions.csv"))

        mean_acc = np.array(acc_list).mean()
        parameters = [activation, kernel_size]
        entries = parameters + acc_list + [mean_acc]

        temp = pd.DataFrame([entries], columns=columns)
        record = record.append(temp, ignore_index=True)

record.to_csv(os.path.join("data", "training_results.csv"))

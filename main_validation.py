import tensorflow as tf
from tensorflow import keras
import pandas as pd
import os
from tqdm import tqdm
import numpy as np

classes = ["date", "analyst name", "time stamp", "test bench", "finding", "unknown"]
corpus = pd.read_pickle(os.path.join('data', 'training_testing_dataset.p'))

corpus.info()

# Separate the sentences and the labels
sentences, labels = list(corpus.sentence), list(corpus.label)

# Define a function to compute the max length of sequence
def max_length(sequences):
    '''
    input:
        sequences: a 2D list of integer sequences
    output:
        max_length: the max length of the sequences
    '''
    max_length = 0
    for i, seq in enumerate(sequences):
        length = len(seq)
        if max_length < length:
            max_length = length
    return max_length


from keras.preprocessing.sequence import pad_sequences

trunc_type = 'post'
padding_type = 'post'
oov_tok = "<UNK>"

# Loading pretrained tokenizer#
import json
with open(os.path.join('data', 'tokenizer_conf.json'), 'r') as file:
    tokenizer_dict = json.load(file)
tokenizer = keras.preprocessing.text.tokenizer_from_json(tokenizer_dict)

# Turn the text into sequence
training_sequences = tokenizer.texts_to_sequences(sentences)
max_len = max_length(training_sequences)

# Pad the sequence to have the same size
training_padded = pad_sequences(training_sequences, maxlen=max_len, padding=padding_type, truncating=trunc_type)

word_index = tokenizer.word_index

from tcn import TCN
models_path = os.path.join("data", "output", "models", "acc_0.992201030254364_relu_3_model.h5")
#models_path = os.path.join("data", "acc_0.9776468276977539_relu_1_model")
model = tf.keras.models.load_model(models_path, custom_objects={"TCN": TCN})
#model = tf.saved_model.load(models_path)

predictions = list()
print("Predictions running:")
for unseen_text, label in tqdm(zip(sentences, labels)):
    if unseen_text != "":
        seq = tokenizer.texts_to_sequences([unseen_text])
        # seq = tokenizer.texts_to_matrix([line_after_line])
        padded = pad_sequences(seq, maxlen=max_len, padding=padding_type, truncating=trunc_type)
        output = model.predict(padded)
        #output = model(padded)
        if np.max(output) > 0.5:
            predictions.append([unseen_text, classes[np.argmax(output)], str(np.max(output)), str(output)])
            print(unseen_text + ", predicted as: " + classes[np.argmax(output)])

predictions_df = pd.DataFrame(predictions, columns=["sentence", "label", "probability"])
predictions_df.to_csv(os.path.join("data", "validation_predictions.csv"))